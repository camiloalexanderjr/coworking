import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserAuthService } from '../services/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class NoAuthGuard implements CanActivate {
  constructor(
    private _userAuth: UserAuthService,
    private _router: Router
  ) { }

  canActivate() {
    if (this._userAuth.getIdentity() && this._userAuth.getToken()) {
      this._router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }

}
