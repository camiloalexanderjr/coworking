import { Component } from '@angular/core';
import { LangsService } from './services/langs.service';
import { Router, ActivationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';
import { TranslatePipe } from '@ngx-translate/core';
import { BackendService } from './services/backend.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent {
  constructor (
    private langsService: LangsService,
    private router: Router,
    private title: Title,
    private meta: Meta,
    private translatePipe: TranslatePipe,
    private backend: BackendService
  ) {
    this.router.events.pipe(
      filter( event => event instanceof ActivationEnd),
      filter( (event: ActivationEnd) => event.snapshot.firstChild === null ),
      map( (event: ActivationEnd) => event.snapshot.data)
    ).
    subscribe(event => {
      if (Object.keys(event).length > 0) {
        this.title.setTitle(this.translatePipe.transform(event.title));
        const metaTag: MetaDefinition = {
          name: 'description',
          content: this.translatePipe.transform(event.description)
        };
        this.meta.updateTag(metaTag);
        for (const metaTags of event.metaTags) {
          const metaT: MetaDefinition = {
            property: metaTags.property,
            content: (metaTags.translate) ? this.translatePipe.transform(metaTags.body) : metaTags.body,
          };
          if (metaTags.image) metaT.itemprop = 'image';
          this.meta.updateTag(metaT);
        }
      } else {
        console.log('nothings');
      }
    });
    this.backend.getViewPlatform()
  }
}
