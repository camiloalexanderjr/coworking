import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecoveryPasswordRoutingModule } from './recovery-password-routing.module';
import { RecoveryPasswordComponent } from './recovery-password.component';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [RecoveryPasswordComponent],
  imports: [
    CommonModule,
    RecoveryPasswordRoutingModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TranslatePipe]
})
export class RecoveryPasswordModule { }
