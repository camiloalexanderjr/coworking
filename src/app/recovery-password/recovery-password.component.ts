import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../services/backend.service';
import { ToastrService } from 'ngx-toastr';
import { TranslatePipe } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.scss']
})
export class RecoveryPasswordComponent implements OnInit {
  token: string;
  myForm: FormGroup;
  successRecovery: boolean;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _backend: BackendService,
    private _toastr: ToastrService,
    private _translate: TranslatePipe,
    private _router: Router
  ) {
    this.token = this._activatedRoute.snapshot.paramMap.get('token');
    this.successRecovery = false;
  }

  ngOnInit(): void {
    this.validToken();
  }
  buildForm() {
    this.myForm = new FormGroup({
      password: new FormControl('',[
        Validators.required,
        Validators.minLength(8)
      ]),
      password_confirmation: new FormControl('', [,
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }
  validToken() {
    this._backend.getvalidTokenPwd(this.token).then(response => {
      this.buildForm();
    }).catch(error => {
      this._toastr.error(this._translate.transform('expired_token'));
      this._router.navigate(['/']);
    });
  }
  onSubmit() {
    if (this.myForm.invalid) return;
    this._backend.postRecoveryPwd(this.token, this.myForm.value).then(response => {
      this.successRecovery = true;
    }).catch(error => {
      this._toastr.error()
    });
  }
}
