import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoAuthGuard } from './guards/no-auth.guard';

const routes: Routes = [
  {
    path: 'recovery-password',
    loadChildren: () => import('./recovery-password/recovery-password.module').then(m => m.RecoveryPasswordModule),
    canActivate: [NoAuthGuard]
  },
  {
    path: '',
    loadChildren: () => import('./platform/platform.module').then(m => m.PlatformModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
