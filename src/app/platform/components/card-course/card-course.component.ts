import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-course',
  templateUrl: './card-course.component.html',
  styles: [
  ]
})
export class CardCourseComponent implements OnInit {
  @Input() course: any;
  constructor() { }

  ngOnInit(): void {
  }

}
