import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MomentModule } from 'angular2-moment';

import { CardCourseComponent } from './card-course/card-course.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    CardCourseComponent
  ],
  imports: [
    CommonModule,
    MomentModule,
    RouterModule
  ],
  exports: [
    CardCourseComponent
  ]
})
export class ComponentsModule { }
