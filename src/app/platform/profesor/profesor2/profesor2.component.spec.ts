import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Profesor2Component } from './profesor2.component';

describe('Profesor2Component', () => {
  let component: Profesor2Component;
  let fixture: ComponentFixture<Profesor2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Profesor2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Profesor2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
