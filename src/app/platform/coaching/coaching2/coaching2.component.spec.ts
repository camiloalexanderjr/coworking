import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Coaching2Component } from './coaching2.component';

describe('Coaching2Component', () => {
  let component: Coaching2Component;
  let fixture: ComponentFixture<Coaching2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Coaching2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Coaching2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
