import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-coaching2',
  templateUrl: './coaching2.component.html',
  styleUrls: ['./coaching2.component.scss']
})
export class Coaching2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  register() {
    $('#registerModal').modal('show');
  }
}
