import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-coaching',
  templateUrl: './coaching.component.html',
  styleUrls: ["../../../assets/css/home.css"]
})
export class CoachingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  register() {
    $('#registerModal').modal('show');
  }
}
