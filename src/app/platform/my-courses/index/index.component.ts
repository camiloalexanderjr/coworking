import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [
  ]
})
export class IndexComponent implements OnInit {
  currentPage: number;
  total: number;
  totalPages: number;
  userCourses: any[];
  loading: boolean;

  constructor(
    private _backend: BackendService
  ) {
    this.loading = true;
    this.currentPage = 1;
    this.total = 0;
    this.totalPages = 0;
    this.userCourses = [];
  }

  ngOnInit(): void {
    this.getUserCourses();
  }
  getUserCourses() {
    this.loading = true;
    this._backend.getMyCourses(this.currentPage).then(response => {
      this.loading = false;
      this.userCourses = response.docs;
      this.total = response.totalDocs;
      this.totalPages = response.totalPages;
      console.log(response);
    }).catch(error => {
      this.loading = false;
      console.error(error);
    });
  }
  setPage(page) {
    if (this.currentPage === page) return;
    this.currentPage = page;
    this.getUserCourses();
  }

}
