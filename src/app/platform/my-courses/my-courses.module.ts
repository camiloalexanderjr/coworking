import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCoursesRoutingModule } from './my-courses-routing.module';
import { IndexComponent } from './index/index.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [IndexComponent],
  imports: [
    CommonModule,
    MyCoursesRoutingModule,
    SharedModule,
    RouterModule,
    TranslateModule
  ],
})
export class MyCoursesModule { }
