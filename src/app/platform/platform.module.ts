import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlatformRoutingModule } from './platform-routing.module';
import { HomeComponent } from './home/home.component';
import { PlatformComponent } from './platform.component';
import { SharedModule } from './shared/shared.module';
import { SliderComponent } from './home/slider/slider.component';
import { Home2Component } from './home/home2/home2.component';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { CoachingComponent } from './coaching/coaching.component';
import { Coaching2Component } from './coaching/coaching2/coaching2.component';
import { RouterModule } from '@angular/router';
import { ProfesorComponent } from './profesor/profesor.component';
import { Profesor2Component } from './profesor/profesor2/profesor2.component';


@NgModule({
  declarations: [
    HomeComponent,
    PlatformComponent,
    SliderComponent,
    Home2Component,
    CoachingComponent,
    Coaching2Component,
    ProfesorComponent,
    Profesor2Component,
  ],
  imports: [
    CommonModule,
    PlatformRoutingModule,
    SharedModule,
    TranslateModule,
    RouterModule
  ],
  providers: [TranslatePipe]
})
export class PlatformModule { }
