import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackendService } from 'src/app/services/backend.service';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  myForm: FormGroup;
  error: any;
  loading: boolean;

  constructor(
    private backend: BackendService,
    private _userAuth: UserAuthService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.buildForm();
    $('#loginModal').on('hide.bs.modal', (e) => {
      this.myForm.reset();
    });
  }

  init() {
    $('#loginModal').modal('show');
  }
  buildForm() {
    this.myForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$')
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  register() {
    this.destroy();
    $('#registerModal').modal('show');
  }
  destroy() {
    $('#loginModal').modal('hide');
  }
  resetPassword() {
    this.destroy();
    $('#resetModal').modal('show');
  }

  onSubmit() {
    if (this.myForm.invalid) return;
    this.backend.postLogin(this.myForm.value).then(response => {
      this.error = null;
      this.loading = true;
      this._userAuth.login(response);
      this.loading = false;
      this.toastr.success('Bienvenido a congenius');
      this.destroy();
    }).catch((error: HttpErrorResponse) => {
      this.loading = false;
      switch(error.status) {
        case 400: {
          this.error = 'Email o contraseña incorrecta';
          this.myForm.controls['password'].setValue('');
          break;
        }
        case 500: {
          this.error = 'Hubo un error en el servidor';
        }
      }
    });
  }
}
