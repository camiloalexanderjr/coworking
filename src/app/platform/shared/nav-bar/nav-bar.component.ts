import { Component, ViewChild, OnInit } from '@angular/core';
import { LoginComponent } from '../login/login.component';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { RegisterComponent } from '../register/register.component';
import { LangsService } from 'src/app/services/langs.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: [
  ]
})
export class NavBarComponent implements OnInit {
  @ViewChild(LoginComponent, { static: false }) loginComponent: LoginComponent;
  @ViewChild(RegisterComponent, { static: false }) registerComponent: RegisterComponent;
  currentLang: any;

  constructor(
    public _userAuth: UserAuthService,
    public lang: LangsService,
    private _activateRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.currentLang = this.lang.datalangs.find( lang => lang.lang === this.lang.currentLang );
  }

  changeLang(lang) {
    this.currentLang = lang;
    this.lang.setLanguage(lang.lang);
  }

  login() {
    this.loginComponent.init();
  }

  register() {
    this.registerComponent.init();
  }

}
