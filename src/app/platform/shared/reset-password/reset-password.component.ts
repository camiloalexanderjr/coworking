import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styles: [
  ]
})
export class ResetPasswordComponent implements OnInit {
  myForm: FormGroup;
  error: string;
  success: boolean;
  constructor(
    private _backend: BackendService
  ) {
    this.init();
  }

  init() {
    this.error = '';
    this.success = false;
  }

  ngOnInit(): void {
    this.buildForm();
    $('#resetModal').on('hide.bs.modal', (e) => {
      this.myForm.reset();
      this.init();
    });
  }
  buildForm() {
    this.myForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$')
      ]),
    });
  }
  onSubmit() {
    if (this.myForm.invalid) return;
    this._backend.postResetPassword(this.myForm.value).then(response => {
      this.success = true;
    }).catch((error: HttpErrorResponse) => {
      switch (error.status) {
        case 400:
          this.error = 'invalid_email';
        break;
        case 404:
          this.error = 'email_not_found';
        break;
      }
    });
  }

}
