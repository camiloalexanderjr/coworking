import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading',
  template: `
    <div class="text-center">
      <div class="spinner-border" [ngClass]="color" role="status" style="width: 5rem; height: 5rem;">
          <span class="sr-only">Loading...</span>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class LoadingComponent {
  @Input() color = 'text-primary';
}
