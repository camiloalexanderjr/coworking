import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BackendService } from 'src/app/services/backend.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit {
  closeResult: string;
  myForm: FormGroup;
  loading: boolean;
  error: string;

  constructor(
    private _backend: BackendService,
    private _userAuth: UserAuthService,
    private _toastr: ToastrService
  ) {
    this.loading = false;
    this.error = null;
  }
  ngOnInit(): void {
    this.buildForm();
  }

  init() {
    $('#registerModal').modal('show');
  }

  login() {
    this.destroy();
    $('#loginModal').modal('show');
  }

  buildForm() {
    this.myForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      surname: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$')]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      password_confirmation: new FormControl('', [Validators.required]),
    });
  }
  onSubmit() {
    if (this.myForm.invalid) return;
    this.loading = true;
    this.error = null;
    this.myForm.disable();
    this._backend.postRegister(this.myForm.value).then(response => {
      this.loading = false;
      this.destroy();
      this._userAuth.login(response);
      this._toastr.success('Bienvenido a Congenius');
    }).catch((error: HttpErrorResponse) => {
      switch(error.status) {
        case 400:
          this.error = 'Formulario inválido';
          break;
        case 404:
          this.error = 'Email ya registrado en la plataforma';
          break;
        case 500:
          this.error = 'Hubo un error en el servidor';
          break;
      }
      this.loading = false;
      this.myForm.enable();
    });
  }
  destroy() {
    $('#registerModal').modal('hide');
    this.myForm.reset();
  }

}
