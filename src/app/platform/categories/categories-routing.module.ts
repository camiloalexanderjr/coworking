import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { NoAuthGuard } from 'src/app/guards/no-auth.guard';


const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'congenius',
      description: 'congenius_description',
      metaTags: [
        { property: 'og:title', body: 'congenius', translate: true },
        { property: 'og:type', body: 'website', translate: false },
        { property: 'og:site_name', body: 'Congenius MS', translate: false },
        { property: 'og:url', body: 'https://congeniusms.com', translate: false },
        { property: 'og:image', body: 'https://congeniusms.com/assets/img/logo-vertical.png', image: true, translate: false },
        { property: 'og:description', body: 'congenius_description', translate: true },
      ]
    }
  },
  { path: ':action', component: IndexComponent, canActivate: [NoAuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
