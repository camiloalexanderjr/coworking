import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { CategoriesRoutingModule } from './categories-routing.module';

import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';

import { IndexComponent } from './index/index.component';
import { CategoryComponent } from './components/category/category.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    IndexComponent,
    CategoryComponent
  ],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    SharedModule,
    ComponentsModule,
    CarouselModule,
    TranslateModule
  ],
})
export class CategoriesModule { }
