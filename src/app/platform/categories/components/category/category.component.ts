import { Component, OnInit, Input } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styles: [
  ]
})
export class CategoryComponent implements OnInit {
  @Input() category: any;
  loading: boolean;
  courses: Array<any>;

  constructor(
    private _backend: BackendService
  ) {
    this.loading = true;
    this.courses = [];
  }

  ngOnInit(): void {
    this.getCourseCategory();
  }
  getCourseCategory() {
    this.loading = true;
    this._backend.getCategoryCourses(this.category._id).then(response => {
      this.loading = false
      this.courses = response;
    }).catch(error => {
      this.loading = false;
    });
  }

}
