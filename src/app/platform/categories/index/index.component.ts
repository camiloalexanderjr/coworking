import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { ActivatedRoute } from '@angular/router';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { OwlOptions } from 'ngx-owl-carousel-o';

declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [
  ]
})
export class IndexComponent implements OnInit {
  loading: boolean;
  error: boolean;
  categories: Array<any>;
  slidesStore :Array<any> = [
    {
      id: 'carousel1',
      src:'./assets/img/carousel/1.jpg',
    },
    {
      id: 'carousel2',
      src:'./assets/img/carousel/2.jpg',
    },
    {
      id: 'carousel3',
      src:'./assets/img/carousel/3.jpg',
    },
    {
      id: 'carousel4',
      src:'./assets/img/carousel/4.jpg',
    },
    {
      id: 'carousel5',
      src:'./assets/img/carousel/5.jpg',
    },
    {
      id: 'carousel6',
      src:'./assets/img/carousel/6.jpg',
    },
  ]
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    autoplay: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
  }

  constructor(
    private backend: BackendService,
    private _activatedRoute: ActivatedRoute,
    private _userAuth: UserAuthService
  ) {
    this.error = false;
    this.loading = true;
    this.categories = [];
    if (!(this._userAuth.getIdentity() && this._userAuth.getToken())) {
      const urlTree = this._activatedRoute.snapshot.paramMap.get('action');
      setTimeout(() => {
        switch (urlTree) {
          case 'register':
            $('#registerModal').modal('show');
          break;
          case 'login':
            console.log('sdsds');
            $('#loginModal').modal('show');
          break;
        }
      }, 1000);
    }
  }

  ngOnInit(): void {
    this.getCategories();
  }
  getCategories() {
    this.loading = true;
    this.backend.getCategories().then(response => {
      this.loading = false;
      this.error = false;
      this.categories = response;
    }).catch(error => {
      this.error = true;
      this.loading = false;
    });
  }

  refresh() {
    this.getCategories();
  }

}
