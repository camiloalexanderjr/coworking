import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ["../../../assets/css/home.css"]
})
export class HomeComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

  register() {
    $('#registerModal').modal('show');
  }
}
