import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['../../../../assets/css/home.css']
})
export class Home2Component implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  register() {
    this.router.navigate(['/ser-profesor']);
    // $('#registerModal').modal('show');
  }
}

