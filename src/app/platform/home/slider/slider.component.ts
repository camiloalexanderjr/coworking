import { Component, OnInit } from '@angular/core';
declare function init_slider();
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  img1: string;
  constructor() { }

  ngOnInit(): void {
    init_slider();
  }

}
