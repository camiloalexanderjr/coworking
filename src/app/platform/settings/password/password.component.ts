import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styles: [
  ]
})
export class PasswordComponent implements OnInit {
  myForm: FormGroup;
  constructor(
    private _backend: BackendService,
    private _toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }
  buildForm() {
    this.myForm = new FormGroup({
      old_password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      new_password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      new_password_confirmation: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }
  onSubmit() {
    if (this.myForm.invalid || this.myForm.value['new_password'] !== this.myForm.value['new_password_confirmation']) return;
    this.myForm.disable();
    this._backend.putPassword(this.myForm.value).then(response => {
      this._toastr.success('Contraseña actualizada correctamente');
      this.myForm.enable();
      this.myForm.reset();
    }).catch(error => {
      this._toastr.error('Ocurrió un error, los datos no se guardaron');
    })
  }
}
