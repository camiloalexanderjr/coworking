import { Component, OnInit } from '@angular/core';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { BackendService } from 'src/app/services/backend.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [
  ]
})
export class IndexComponent implements OnInit {
  myForm: FormGroup;

  constructor(
    private _userAuth: UserAuthService,
    private _backend: BackendService,
    private _toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }
  buildForm() {
    const userData = this._userAuth.getIdentity();
    this.myForm = new FormGroup({
      name: new FormControl(userData.name, Validators.required),
      surname: new FormControl(userData.surname, [Validators.required]),
      description: new FormControl((userData.description) ? userData.description : ''),
    });
  }
  onSubmit() {
    if (this.myForm.invalid) return;
    this._backend.putUser(this.myForm.value).then(response => {
      this._toastr.success('Datos Actualizados con éxito');
      this._userAuth.setUser(response);
    }).catch(error => {
      this._toastr.error('Ocurrió un error, tus datos no se guardaron');
    })
  }
}
