import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { SettingsComponent } from './settings.component';
import { ImageComponent } from './image/image.component';
import { PasswordComponent } from './password/password.component';


const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      { path: 'informacion', component: IndexComponent },
      { path: 'imagen', component: ImageComponent },
      { path: 'contrasena', component: PasswordComponent },
      { path: '**', pathMatch: 'full', redirectTo: 'informacion' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
