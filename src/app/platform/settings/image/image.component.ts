import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styles: [
  ]
})
export class ImageComponent implements OnInit {

  afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg,.png,.jpeg',
    maxSize: '5',
    uploadAPI:  {
      url: `${ this._backend.url }/user/image`,
      headers: {
        authorization : this._userAuth.getToken()
      }
    },
    theme: 'attachPin',
    hideResetBtn: true,
  };
  constructor(
    private _backend: BackendService,
    public _userAuth: UserAuthService,
    private _toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  responseUpload(event: XMLHttpRequest) {
    if (event.status === 200) {
      const response = JSON.parse(event.response);
      setTimeout(() => {
        this._userAuth.setUser(response);
      }, 2000);
      console.log(event);
      this._toastr.success('Se ha subido el vídeo de la lección');
    } else {
      this._toastr.error('Ocurrió un error, tus datos no se guardaron');
    }
  }


}
