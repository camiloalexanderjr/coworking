import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { AngularFileUploaderModule } from "angular-file-uploader";

import { SettingsRoutingModule } from './settings-routing.module';
import { IndexComponent } from './index/index.component';
import { SettingsComponent } from './settings.component';
import { ImageComponent } from './image/image.component';
import { PasswordComponent } from './password/password.component';


@NgModule({
  declarations: [
    IndexComponent,
    SettingsComponent,
    ImageComponent,
    PasswordComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule,
    FroalaViewModule,
    AngularFileUploaderModule,
  ]
})
export class SettingsModule { }
