import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from './show/show.component';
import { LessonComponent } from './lesson/lesson.component';
import { AuthGuard } from 'src/app/guards/auth.guard';


const routes: Routes = [
  { path: ':path', component: ShowComponent },
  { path: ':path/clases', component: LessonComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }
