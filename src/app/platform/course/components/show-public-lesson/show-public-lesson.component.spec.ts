import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPublicLessonComponent } from './show-public-lesson.component';

describe('ShowPublicLessonComponent', () => {
  let component: ShowPublicLessonComponent;
  let fixture: ComponentFixture<ShowPublicLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowPublicLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPublicLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
