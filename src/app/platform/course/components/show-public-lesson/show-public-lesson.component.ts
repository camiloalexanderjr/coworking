import { Component } from '@angular/core';

declare var $: any;
declare var videojs : any ;

@Component({
  selector: 'app-show-public-lesson',
  templateUrl: './show-public-lesson.component.html',
  styleUrls: ['./show-public-lesson.component.scss']
})
export class ShowPublicLessonComponent {
  lesson: any;
  player: any;
  constructor() {
    this.lesson = null;
  }


  initLesson(lesson: any) {
    $('#viewLessonModal').modal('show');
    this.lesson = lesson;
    console.log(this.lesson);
    setTimeout(() =>{
      this.player = videojs(document.getElementById('video-player-lesson'), {
        fluid: true,
        sources:  {
          src: 'https://congenius.s3.us-east-2.amazonaws.com/lessons/' + this.lesson.body,
        }
      });
      setTimeout(() => {
        this.player.play();
      }, 200);
    }, 500);
  }
  close() {
    this.lesson = null;
    $('#viewLessonModal').modal('hide');
  }
}
