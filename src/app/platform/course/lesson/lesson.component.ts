import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BackendService } from 'src/app/services/backend.service';
import { ToastrService } from 'ngx-toastr';

declare var videojs : any ;

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styles: [
  ]
})
export class LessonComponent implements OnInit {

  loading: boolean;
  observable: Subscription;
  path: string;
  course: any;
  actualLesson: any;
  actualsection: any;
  player: any ;
  video: boolean;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _backend: BackendService,
    private _toastr: ToastrService,
    private _router: Router
  ) {
    this.loading = true;
    this.actualLesson = null;
    this.actualsection = null;
  }

  ngOnInit(): void {
    this.path = this._activatedRoute.snapshot.params.path;
    this.getCourse();
  }
  getCourse() {
    this.loading = true;
    this._backend.getMyCourse(this.path).then(response => {
      this.course = response;
      this.loading = false;
      this.changeActualLesson(this.course.sections[0].lessons[0], this.course.sections[0]);
    }).catch(error => {
      console.log(error);
      this._toastr.error('Ocurrió un error con tu curso, redireccionado al inicio');
      this._router.navigate(['/']);
    });
  }

  changeActualLesson(lesson: any, section: any) {
    if (this.actualLesson === lesson) return;
    if (this.actualLesson !== section) this.actualsection = section;
    this.actualLesson = lesson;
    if (this.actualLesson.is_video) {
      this.video = false;
      setTimeout(() => {
        this.video = true;
        setTimeout(() => {
          this.player = videojs(document.getElementById('video-player'), {
            fluid: true,
            sources:  {
              src: 'https://congenius.s3.us-east-2.amazonaws.com/lessons/' + this.actualLesson.body,
            }
          });
          setTimeout(() => {
            this.player.play();
          }, 200);
        }, 50);
      }, 50);
    }
  }

}
