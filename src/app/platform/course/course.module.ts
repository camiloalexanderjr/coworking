import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseRoutingModule } from './course-routing.module';
import { ShowComponent } from './show/show.component';
import { SharedModule } from '../shared/shared.module';
import { LessonComponent } from './lesson/lesson.component';
import { ShowPublicLessonComponent } from './components/show-public-lesson/show-public-lesson.component';


@NgModule({
  declarations: [ShowComponent, LessonComponent, ShowPublicLessonComponent,],
  imports: [
    CommonModule,
    CourseRoutingModule,
    SharedModule,
    TranslateModule
  ],
  providers: [TranslatePipe]
})
export class CourseModule { }
