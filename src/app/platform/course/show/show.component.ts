import { ShowPublicLessonComponent } from './../components/show-public-lesson/show-public-lesson.component';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { UserAuthService } from 'src/app/services/user-auth.service';
import { environment } from 'src/environments/environment';

declare var videojs : any ;
declare var $: any;
declare var ePayco: any;
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styles: [
  ]
})
export class ShowComponent implements OnDestroy {
  @ViewChild(ShowPublicLessonComponent, { static: true }) showPublicLessonComponent: ShowPublicLessonComponent
  player: any ;
  loading: boolean = true;
  path: string;
  course: any;
  sub: any;
  totalLessons: number;
  totalArticles: number;
  lessonNumber: number;
  loadingOrder: boolean;
  dataPay: any;
  courseAcquired: boolean;

  constructor(
    private _backend: BackendService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _toastr: ToastrService,
    private _userAuth: UserAuthService
  ) {
    this.sub = this._activatedRoute.params.subscribe(params => {
      this.courseAcquired = false;
      this.dataPay = null;
      this.loadingOrder = false;
      this.course = null;
      this.totalLessons = 0;
      this.totalArticles = 0;
      this.lessonNumber = 0;
      this.path = params['path'];
      this.getCourse();
    });
  }

  getCourse() {
    this.loading = true;
    this._backend.getCourse(this.path).then(response => {
      this.loading = false;
      this.course = response;
      this._backend.getViewCourse(this.course._id);
      this.validMyCourse();
      setTimeout(() => this.initIntroduction(), 500);
      for (const section of this.course.sections) {
        this.totalLessons += section.lessons.length;
        for (const lesson of section.lessons) {
          if (!lesson.is_video) this.totalArticles += 1;
        }
      }
    }).catch((error: HttpErrorResponse) => {
      this.loading = false;
      switch (error.status) {
        case 404 :
          this._toastr.error('Curso no encontrado o inhabilitado, redirecionado al inicio');
          this._router.navigate(['/']);
          break;
      }
    })
  }
  validMyCourse() {
    if (this._userAuth.getIdentity() && this._userAuth.getToken()) {
      this._backend.getValidMyCourse(this.path).then(response => {
        this.courseAcquired = true;
      }).catch(error => { });
    }
  }
  initIntroduction() {
    this.player = videojs(document.getElementById('video-player'), {
      fluid: true,
      sources:  {
        src: 'https://congenius.s3.us-east-2.amazonaws.com/introductions/' + this.course.introduction,
      }
    }
    );
    setTimeout(() => {
      this.player.play();
    }, 200);
  }

  generateOrder() {
    if (this._userAuth.getToken() && this._userAuth.getIdentity()) {
      this.loadingOrder = true;
      this._backend.postOrder(this.course._id).then(response => {
        this.dataPay = response;
        let handler = ePayco.checkout.configure({
          key: this.dataPay.pay_data.key,
          test: !environment.production
        });
        let data = {
          external: false,
          name: this.dataPay.pay_data.description,
          amount: this.dataPay.order.expected_amount,
          currency: this.dataPay.pay_data.currency,
          invoice: this.dataPay.order._id,
          response: 'https://congeniusms.com/',
          confirmation: `${ environment.backend }/order/confirmation/epayco`,
          name_billing: this.dataPay.pay_data.buyerFullName,
        };
        handler.open(data)
      }).catch(error => {
        this._toastr.error('Ocurrió un error en la compra de la orden');
      });
    } else {
      $('#loginModal').modal('show');
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  showPublicLesson(lesson: any) {
    this.showPublicLessonComponent.initLesson(lesson);
  }
}
