import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlatformComponent } from './platform.component';
import { AuthGuard } from '../guards/auth.guard';
import { CoachingComponent } from './coaching/coaching.component';
import { ProfesorComponent } from './profesor/profesor.component';


const routes: Routes = [
  {
    path: '',
    component: PlatformComponent,
    children: [
      {
        path: 'mis-cursos',
        loadChildren: () => import('./my-courses/my-courses.module').then(m => m.MyCoursesModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'curso',
        loadChildren: () => import('./course/course.module').then(m => m.CourseModule),
      },
      {
        path: 'ajustes',
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
        canActivate: [AuthGuard]
      },
      {
        path: '',
        // loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule)
        component: HomeComponent
      },
      {
        path: 'ser-profesor',
        component: ProfesorComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlatformRoutingModule { }
