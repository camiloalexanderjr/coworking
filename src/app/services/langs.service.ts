import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LangsService {
  langs: Array<string>;
  datalangs: Array<any>;
  currentLang: string;

  constructor(
    private translate: TranslateService
  ) {

    this.langs = [
      'es',
      // 'en'
    ];
    this.datalangs = [
      {
        lang: 'es',
        title: 'Español',
        flag: 'flag-icon-es'
      },
      // {
      //   lang: 'en',
      //   title: 'English',
      //   flag: 'flag-icon-us'
      // },
    ];
    this.initAplication();
  }

  initAplication() {
    this.translate.addLangs(this.langs);
    let lang = localStorage.getItem('lang');
    if (!lang ||  this.langs.find(langS => langS === lang) === undefined ) {
      lang = 'es';
      localStorage.setItem('lang', lang);
    }
    this.translate.setDefaultLang(lang);
    this.currentLang = lang;
  }

  setLanguage(lang: string) {
    if (this.langs.find(langS => langS === lang) === undefined) return;
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
    this.currentLang = lang;
  }
}
