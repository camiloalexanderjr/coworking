import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {

  constructor(
    private _router: Router
  ) { }

  login(dataUser: any) {
    this.setToken(dataUser.token);
    this.setUser(dataUser.user);
  }

  logout() {
    const unprotectedUrls = ['/', '/categorias'];
    const actualUrlValid = unprotectedUrls.findIndex(u => u == this._router.url);
    localStorage.removeItem(environment.token);
    localStorage.removeItem(environment.user);
    if (actualUrlValid < 0) this._router.navigate(['/']);
    console.log(actualUrlValid);
    console.log(this._router.url);
  }

  setToken(token: string) {
    localStorage.setItem(environment.token, token);
  }

  setUser(user: object) {
    localStorage.setItem(environment.user, JSON.stringify(user));
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem(environment.user));
    if(identity == 'undefined') {
      identity = null;
    }
    return identity;
  }

  getToken() {
    let token = localStorage.getItem(environment.token);
    if(token == 'undefined') {
      token = null;
    }
    return token;
  }
}
