import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { UserAuthService } from './user-auth.service';
import { environment } from 'src/environments/environment';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BackendService {
  url: string;

  constructor(
    private http: HttpClient,
    private userAuth: UserAuthService
  ) {
    this.url = environment.backend;
  }

  private postQuery(query: string, params: any, token: boolean = true): Promise<any>  {
    const url = `${this.url}${query}`;
    let headers: any;
    if (token) {
      headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization', this.userAuth.getToken());
    } else {
      headers = new HttpHeaders().set('Content-Type', 'application/json');
    }
    return this.http.post(url, params, { headers } ).pipe(
      catchError(err => this.catchError(err))
    ).toPromise();
  }

  private putQuery(query: string, params: any, token: boolean = true): Promise<any>  {
    const url = `${this.url}${query}`;
    let headers: any;
    if (token) {
      headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization', this.userAuth.getToken());
    } else {
      headers = new HttpHeaders().set('Content-Type', 'application/json');
    }
    return this.http.put(url, params, { headers } ).pipe(
      catchError(err => this.catchError(err))
    ).toPromise();
  }

  private getQuery(query: string, token: boolean = true ): Promise<any> {
    const url = `${this.url}${query}`;
    let headers: any;
    if (token) {
      headers = new HttpHeaders().set('Content-Type', 'application/json')
        .set('Authorization', this.userAuth.getToken());
    } else {
      headers = new HttpHeaders().set('Content-Type', 'application/json');
    }
    return this.http.get(url, { headers } ).pipe(
      catchError(err => this.catchError(err))
    ).toPromise();
  }

  private catchError(err: HttpErrorResponse ) {
    if (err.status === 403 || err.status === 401) {
      // this.userAuth.logout();
      console.log('token invalido o ususario invalido');
    }
    return throwError(err);
  }

  getViewPlatform = () => this.getQuery('/view/platform', false)
  getViewCourse = course => this.getQuery(`/view/course/${ course }`, false)
  getCategories = () => this.getQuery('/category', false)
  getCategoryCourses = category => this.getQuery(`/course-category/category/${ category }`, false)
  getCourse = path => this.getQuery(`/course/${ path }/client`, false)
  getMyCourses = page => this.getQuery(`/user-course?page=${ page }`)
  getValidMyCourse = path => this.getQuery(`/user-course/${ path }`)
  getvalidTokenPwd = token => this.getQuery(`/user/forgot-password/${ token }`, false)
  getMyCourse = path => this.getQuery(`/course/${ path }/sections`)
  postLogin = dataLogin => this.postQuery('/user/login', dataLogin, false)
  postOrder = course => this.postQuery(`/order/course/${ course }`, {})
  postRecoveryPwd = (token, dataReco) => this.postQuery(`/user/password/${ token }`, dataReco, false)
  postRegister = dataRegister => this.postQuery('/user/register', dataRegister, false)
  postResetPassword = dataReset => this.postQuery('/user/forgot-password', dataReset, false)
  putUser = dataUser => this.putQuery('/user/information', dataUser)
  putPassword = dataPwd => this.putQuery('/user/password', dataPwd)
}
